class IndecisionApp extends React.Component {
    render() {
        const title = "Indecision"
        const subtitle = "Put your life in the hands of a computer."
        const options = ['Thing1', 'Thing2', 'Thing3', 'Thing4']

        return (
            <div className="container">
                <Header title={title} subtitle={subtitle} />
                <Action />
                <Options options={options} />
                <AddOption />
            </div>
        )
    }
}

class Header extends React.Component {
    render() {
        return (
            < div >
                <h1>{this.props.title}</h1>
                <h2>{this.props.subtitle}</h2>
            </div >
        )
    }
}

class Action extends React.Component {
    render() {
        return (
            <button>What should I do?</button>
        )
    }
}

class Options extends React.Component {
    render() {
        return (
            <div>
                <p>options here</p>
                {
                    this.props.options.map(option => <Option key={option} optionText={option} />)
                }
            </div>
        )
    }
}

class Option extends React.Component {
    render() {
        return (
            <div>
                <p>{this.props.optionText}</p>
            </div>
        )
    }
}

class AddOption extends React.Component {
    render() {
        return (
            <form>
                <p>add option component</p>
                <input type="text" />
                <button>Add Option</button>
            </form>
        )
    }
}

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));