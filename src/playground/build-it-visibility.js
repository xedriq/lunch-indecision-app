let hidden = true;

const handleShow = () => {
  hidden = !hidden;
  renderApp();
};

const renderApp = () => {
  const template = (
    <div>
      <h1>Visibility Toggle</h1>
      <button onClick={handleShow}>
        {hidden ? "Show Details" : "Hide Details"}
      </button>
      {!hidden && <p>Hidden Details</p>}
    </div>
  );
  ReactDOM.render(template, document.getElementById("app"));
};

renderApp();
