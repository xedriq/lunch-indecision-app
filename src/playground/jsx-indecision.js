

const app = {
    title: "Lunch Indecision App",
    subtitle: "Put your lunch choice in the hands of a computer.",
    options: ["Karinderia ni Manang", "7-11", "Mini stop"]
};

const onSubmit = e => {
    e.preventDefault();
    const option = e.target.elements.option.value;
    if (option) {
        app.options.push(option);
        e.target.elements.option.value = "";
        renderApp();
    }
};

const onRemoveAll = () => {
    app.options = [];
    renderApp();
};

const onMakeDecision = () => {
    const randumNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randumNum];
    alert(option);
};

const appRoot = document.getElementById("app");

const renderApp = () => {
    const template = (
        <div className="text-center mt-5 container p-md-5">
            <h1>{app.title}</h1>
            {app.subtitle && <p className="lead">{app.subtitle}</p>}
            <p>{app.options.length > 0 ? "Here are your options" : "No options"}</p>
            <ol className="list-group mx-auto bg-light">
                {app.options.map(option => (
                    <li className="list-group-item" key={option}>
                        {option}
                    </li>
                ))}
            </ol>
            <button
                className="btn btn-primary"
                onClick={onMakeDecision}
                disabled={!app.options.length}
            >
                What / Where Should We Eat?
        </button>
            <button className="btn btn-warning" onClick={onRemoveAll}>
                Remove All
        </button>
            <form onSubmit={onSubmit}>
                <div className="form-group">
                    <input
                        className="form-control text-center mx-auto"
                        type="text"
                        name="option"
                    />
                    <button className="btn btn-success">Add Option</button>
                </div>
            </form>
        </div>
    );
    ReactDOM.render(template, appRoot);
};

renderApp();
