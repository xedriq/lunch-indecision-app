class Person {
    constructor(name = 'Anonymous', age = 0) {
        this.name = name
        this.age = age
    }

    getGreeting() {
        return `Hello, I am ${this.name}!`
    }

    getDescription() {
        return `${this.name} is ${this.age} year(s) old.`
    }
}


class Student extends Person {
    constructor(name, age, major) {
        super(name, age)
        this.major = major
    }

    hasMajor() {

    }
}

const stud1 = new Student('cedrick', 36, 'Computer Science')
console.log(stud1)